/**
 * @author Alexander Entinger, MSc / LXRobotics
 * @brief this file implements an arduino library to interface the LXRobotics Brushless Motorshield
 * @file BLDC.c
 * @license Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0) ( http://creativecommons.org/licenses/by-nc-sa/3.0/ )
 */

#include "BLDC.h"

/* C IMPLEMENTATION */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <stdbool.h>

// enabling this defines puts the signal of the comparator isr on the digital pin D9 (PB1) ... useful for triggering the oszillocop
//#define BRUSHLESS_DEBUG_OSZI

/* HARDWARE ABSTRACTION LAYER */
// UH = D32 = PC5
#define UH_DDR		(DDRD)
#define UH_PORT		(PORTD)
#define UH			(1<<5)
// UL = D33 = PC4
#define UL_DDR		(DDRD)
#define UL_PORT		(PORTD)
#define UL			(1<<4)
// VH = D34 = PC3
#define VH_DDR		(DDRD)
#define VH_PORT		(PORTD)
#define VH			(1<<3)
// VL = D35 = PC2
#define VL_DDR		(DDRD)
#define VL_PORT		(PORTD)
#define VL			(1<<2)
// WH = D36 = PC1
#define WH_DDR		(DDRD)
#define WH_PORT		(PORTD)
#define WH			(1<<1)
// WL = D37 = PC0
#define WL_DDR		(DDRD)
#define WL_PORT		(PORTD)
#define WL			(1<<0)
// MUX settings for connecting the bemf pin to the AIN1 input of the analog comparator
// BEMF_U = A3 = ADC3 // 011
#define MUX_BEMF_U ADMUX &= 0xF0; ADMUX |= (1<<MUX1) | (1<<MUX0);
// BEMF_V = A4 = ADC4 // 100
#define MUX_BEMF_V ADMUX &= 0xF0; ADMUX |= (1<<MUX2);
// BEMF_W = A5 = ADC5 // 101
#define MUX_BEMF_W ADMUX &= 0xF0; ADMUX |= (1<<MUX2) | (1<<MUX0);

#define _SET(PORT, BIT) (PORT) |= (BIT)
#define _CLEAR(PORT, BIT) (PORT) &= ~(BIT)

/* GLOBAL VARIABLES */
static volatile int8_t m_state = 0;
static volatile uint8_t m_speed = 0;
static volatile bool m_startup_complete = false;
static volatile E_DIRECTION m_dir = FORWARD;

/* PROTOTYPE SECTION */
void C_set_pins_according_to_state();

/* PROGRAM SECTION */

/**
 * @brief initializes the brushless motor control
 */
void C_init_brushless_motor_control() {
  // set all transistor to Low
  _CLEAR(UH_PORT, UH);
  _CLEAR(UL_PORT, UL);
  _CLEAR(VH_PORT, VH);
  _CLEAR(VL_PORT, VL);
  _CLEAR(WH_PORT, WH);
  _CLEAR(WL_PORT, WL);
  // set all IN_x ports to outputs
  _SET(UH_DDR, UH);
  _SET(UL_DDR, UL);
  _SET(VH_DDR, VH);
  _SET(VL_DDR, VL);
  _SET(WH_DDR, WH);
  _SET(WL_DDR, WL);

  // set the pins according to the current state
  C_set_pins_according_to_state();

  // init the analog comparator
  // turn off adc
  ADCSRA &= ~(1<<ADEN);
  // turn off the digital input buffer on the pin AIN0 (PD6, SUM Signal)
  DIDR1 |= (1<<AIN0D);
  // turn off the digital input buffer on the pins ADC5, ADC4, ADC3
  DIDR0 |= (1<<ADC5D) | (1<<ADC4D) | (1<<ADC3D);
  // use multiplexer for negative comparator input
  ADCSRB |= (1<<ACME);
  // analog comparator is enabled
  ACSR &= ~(1<<ACD);
  // trigger interrupt on falling edge of comparator
  ACSR |= (1<<ACIS1);
  // enable analog comparator interrupt
  ACSR |= (1<<ACIE);

  // init timer 2 (for pwm generation) - output is activated on timer overflow, deactivated at compare match
  // clear TCCR2A
  TCCR2A = 0x00;
  // clear TCNT2
  TCNT2 = 0;
  // enable timer 2 overflow and compare match a interrupt
  TIMSK2 |= (1<<TOIE2) | (1<<OCIE2A);
  // set compare match register to zero
  OCR2A = 0;
  // prescaler = 1, 1 Timerstep = 500 ns, 255 Timersteps = 127.5 usm, fPWM = 7,8 kHz
  TCCR2B = (1<<CS20);

  // init watchdog as timeout timer
  WDTCSR = (1<<WDCE) | (1<<WDIE) | (1<<WDP2) | (1<<WDP1);
}

/**
 * @brief sets the speed for the brushless motor
 * @param speed
 */
void C_set_speed(uint8_t const speed) {
  m_speed = speed;
  OCR2A = m_speed;
  if(m_speed == 0) TCCR2B &= ~(1<<CS20);
  else TCCR2B |= (1<<CS20);
}

/**
 * @brief this functions sets the output and inputs according to the motor state
 */
void C_set_pins_according_to_state() {
  switch(m_state) {
  case 0:
    {
      if(m_dir == FORWARD) {
        // U = PWM, V = Float, W = GND
        _SET(UH_PORT, UH);
        _CLEAR(VL_PORT, VL);
        MUX_BEMF_V;
        _SET(WL_PORT, WL);
      } else {
        // U = Float, V = PWM, W = GND
      }
    }
    break;
  case 1:
    {
      if(m_dir == FORWARD) {
        // U = Float, V = PWM, W = GND
        _CLEAR(UH_PORT, UH);
        MUX_BEMF_U;
        _SET(VH_PORT, VH);
        _SET(WL_PORT, WL);
      } else {
         // U = PWM, V = Float, W = GND
      }
    }
    break;
  case 2:
    {
      if(m_dir == FORWARD) {
        // U = GND, V = PWM, W = Float
        _SET(UL_PORT, UL);
        _SET(VH_PORT, VH);
        _CLEAR(WL_PORT, WL);
        MUX_BEMF_W;
      } else {
        // U = PWM, V = GND, W = Float
      }
    }
    break;
  case 3:
    {
      if(m_dir == FORWARD) {
        // U = GND, V = Float , W =  PWM
        _SET(UL_PORT, UL);
        _CLEAR(VH_PORT, VH);
        MUX_BEMF_V;
        _SET(WH_PORT, WH);
      } else {
        // U = Float, V = GND , W =  PWM
      }
    }
    break;
  case 4:
    {
      if(m_dir == FORWARD) {
        // U = Float, V = GND, W = PWM
        _CLEAR(UL_PORT, UL);
        MUX_BEMF_U;
        _SET(VL_PORT, VL);
        _SET(WH_PORT, WH);
      } else {
        // U = GND, V = Float, W = PWM
      }
    }
    break;
  case 5:
    {
      if(m_dir == FORWARD) {
        // U = PWM, V = GND, W = Float
        _SET(UH_PORT, UH);
        _SET(VL_PORT, VL);
        _CLEAR(WH_PORT, WH);
        MUX_BEMF_W;
      } else {
        // U = GND, V = PWM, W = Float
      }
    }
    break;
  default:
    {

    }
    break;
  }
}

/**
 * @brief interrupt service routine for analog comparator interrupt
 */
ISR(ANALOG_COMP_vect) {
  static uint8_t startup_isr_cnt = 0;
#ifdef BRUSHLESS_DEBUG_OSZI
  AC_INT_PORT |= AC_INT;
#endif
  if(m_startup_complete) {
    // switch to the next state
    m_state++;
    if(m_state == 6) m_state = 0;
    // and set the pins according to the new state
    C_set_pins_according_to_state();
    // toogle on wich egde the ac is triggering
    ACSR ^= (1<<ACIS0);
  }
  else {
    startup_isr_cnt++;
    if(startup_isr_cnt > 25) {
      m_startup_complete = true;
      startup_isr_cnt = 0;
      m_state = 0;
    }
  }
  // reset timer 2
  TCNT2 = 0;
  // reset watchdog here, if watchdog is reset here regularly, it shows that the motor is always running
  wdt_reset();
}

/**
 * @brief interrupt service routine for timer 2 overflow interrupt
 */
ISR(TIMER2_OVF_vect) {
#ifdef BRUSHLESS_DEBUG_OSZI
  AC_INT_PORT &= ~AC_INT;
#endif

  // activate current pwm pin
  switch(m_state) {
  case 0:
    _SET(UH_PORT, UH);
    break; // U = PWM
  case 1:
    _SET(VH_PORT, VH);
    break; // V = PWM
  case 2:
    _SET(VH_PORT, VH);
    break; // V = PWM
  case 3:
    _SET(WH_PORT, WH);
    break; // W = PWM
  case 4:
    _SET(WH_PORT, WH);
    break; // W = PWM
  case 5:
    _SET(UH_PORT, UH);
    break; // U = PWM
  default:
    break;
  }
}

/**
 * @brief interrupt service routine for timer 2 compare match a interrupt
 */
ISR(TIMER2_COMPA_vect) {
  // deactivate current pwm pin
  switch(m_state) {
  case 0:
    _CLEAR(UH_PORT, UH);
    break; // U = PWM
  case 1:
    _CLEAR(VH_PORT, VH);
    break; // V = PWM
  case 2:
    _CLEAR(VH_PORT, VH);
    break; // V = PWM
  case 3:
    _CLEAR(WH_PORT, WH);
    break; // W = PWM
  case 4:
    _CLEAR(WH_PORT, WH);
    break; // W = PWM
  case 5:
    _CLEAR(UH_PORT, UH);
    break; // U = PWM
  default:
    break;
  }
}

/**
 * @brief watchdog isr: if this occurs the motor did either never start or was forcefully stopped
 */
ISR(WDT_vect) {
  wdt_reset();
  WDTCSR |= (1<<WDCE) | (1<<WDIE); // reenable interrupt to prevent system reset
  if(m_speed > 0) {
    sei();
    m_startup_complete = false;
    while(!m_startup_complete) {
      // switch to the next state
      m_state++;
      if(m_state == 6) m_state = 0;
      // and set the pins according to the new state
      C_set_pins_according_to_state();
      // wait a little
      _delay_ms(10);
    }
  }
}

/* ARDUINO INTERFACE */

/**
 * @brief initialize the brushless shield
 */
void BLDC::begin() {
  C_init_brushless_motor_control();
}

/**
 * @brief sets the speed of the brushless motor
 * @param speed value between 0 and 255, where 0 is stop and 255 is full speed
 */
void BLDC::set_speed(uint8_t const speed) {
  C_set_speed(speed);
}

/**
 * @brief returns the current speed
 */
uint8_t BLDC::get_speed() {
  return m_speed;
}

/**
 * @brief sets the direction of the brushless motor
 */
void BLDC::set_direction(E_DIRECTION const dir) {
  m_dir = dir;
}

/**
 * @brief sets the direction of the brushless motor
 */
E_DIRECTION BLDC::get_direction() {
  return m_dir;
}
