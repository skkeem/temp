/**
 * @author Alexander Entinger, MSc / LXRobotics
 * @brief this file demonstrates how to use the brushless motor
 * @file brushless_demo.ino
 */

#include "BLDC.h"

void setup() {
  BLDC::begin();
  BLDC::set_direction(FORWARD);
  Serial.begin(115200);
}

void loop() {
  BLDC::set_direction(FORWARD);
  ramp_up();
  ramp_down();
  BLDC::set_direction(BACKWARD);
  ramp_up();
  ramp_down();
}

/**
 * @brief increases the speed from 0 to 255
 */
void ramp_up() {
  for(int s = 0; s < 255; s+=5) {
    BLDC::set_speed(s);
    Serial.print("Speed = ");
    Serial.println(s);
    delay(100);
  }
}

/**
 * @brief reduces the speed from 255 to 0
 */
void ramp_down() {
  for(int s = 255; s > 0; s-=5) {
    BLDC::set_speed(s);
    Serial.print("Speed = ");
    Serial.println(s);
    delay(100);
  }
}


